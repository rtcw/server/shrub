================================================================================
shrubmod for Return to Castle Wolfenstein 1.4/1.41
--------------------------------------------------------------------------------
Version 3.1.1b, Released 5/10/03
by Ryan Mannion - "shrub" - shrub@planetwolfenstein.com

http://www.planetwolfenstein.com/shrub
================================================================================
NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE

This version's documentation was rushed together in the early morning and I
probably left some features undocumented.  Check the web site for any updates to
this file.

Also, I did not include the sound pack in this release.  If you do not have it,
you will want to download it from my site.
================================================================================

YOU MUST READ AND AGREE TO THE TERMS OF THE LICENSE IN SECTION 7 BEFORE USING
THE INCLUDED .DLL OR .SO FILE.  IT'S VERY REASONABLE, AND I THINK YOU'LL AGREE.

Note that this *is* a beta release, and I would appreciate any and all bug
reports you have.

This mod has been renamed to simply "shrubmod", as it is clear that its
usefulness has expanded well beyond OLTL servers.

What does this mod do?  To put it simply, I designed it primarily to give admins
increased control over every aspect of gameplay so that they may craft a unique
form of playing.  These controls include how points are assigned, what weapons
are allowed, what kind of actions are permissible, and how various in-game
objects are handled.  Second are conveniences for the player, including such
functions as shoving other players out of the way, comprehensive statistics,
automated reminders based on team composition, automated recommendations of
who should revive (which medic is closest) and who to give ammo to (who has the
greatest need, while factoring in distance from the LT), and more!

This mod is completely server-side.  The beauty of it is that it requires no
client downloads whatsoever.

Since there are so many customizable features in this mod, I have set the
variables to a default setting that I think would be a great foundation to start
from.

As always, I'm open to suggestions!  I can be reached at the email address above.

Contents of this Document:
  1. Revision History
  2. Installation
  3. Acknowledgements
  4. Command Summary
  5. What's New/Modified in 3.1b
  6. What's New/Modified in 3.0b
  7. Features Unchanged Prior to 3.0b
  8. Known Bugs
  9. License

================================================================================
1. REVISION HISTORY
================================================================================
Beta 3.1.1 - released 5/10/03
  - fixed problem with disguises and class times
  - removed debug output

Beta 3.1 - released 5/10/03
  - added accurate/overheating MG42s
  - added text shortcuts
  - added g_highachieversmin
  - added panzerfaust arc
  - added party panzer
  - added trip mines
  - added additional sound pack commands
  - added unrestricted sound support
  - added "poll" vote
  - added "putspecs" vote
  - added disguises
  - added logfile trimming
  - added MP40/Thompson rate of fire control
  - added kickable airstrikes
  - added hitsounds
  - added relaxed air strike blocking
  - added ability to zero LT charge on round start
  - added ability to adjust ammo/grenade pack throw rate
  - added customizable objective spawning
  - added cpclient
  - changed warmup damage to allow instant respawn and no damage taken for
    idlers
  - changed logfile output
  - added detailed end of round stats table
  - reinstated unlag code
  - added ability to walk or crouch throw proximity mines
  - added ability to show votes
  - vote successes/failures are logged
  - importance levels are now flagged
  - dropped objs no longer give extra points
  - fixed maxlives/overtime bug
  - fixed ammo bug
  - fixed late join/limited life bug with g_allowLateJoins
  - fixed shove kills logging
  - fixed crash exploit

Beta 3.0.1 - released 1/2/03
  - fixed exploit

Beta 3.0 - released 1/1/03
  - added ready-up system
  - added anti-shove code to prevent shoving teammates to their death
  - added smoke grenades
  - added grenade packs
  - added poison needles
  - added mine hints
  - added weapon unlocking
  - added goomba killing
  - added throwable knives
  - added LT binocular drop on limbo
  - added medic health pack(s) drop on limbo
  - added engineer grenade(s) drop on limbo
  - added chat anti-repeat-spamming protection
  - added rcon lite
  - added high achievers announcements
  - added important flag
  - added poisoning/shoving/goomba deaths logging
  - added objective dropping
  - added sound pack support
  - added death/flamethrower sounds
  - added spectator clicking
  - added g_corpsewar
  - added multiclass
  - added variable maxlives through g_allowLateJoins
  - team counts now displayed in /classes
  - start of round/warmup is now logged
  - added console command "chatclient"
  - added client command "me"
  - chat/chatclient/cp/print are now logged
  - disabled giving ammo from pistols for g_giveammo 2
  - added "On my way" voice chat
  - headshot mode icons now face the shooter
  - modified display of round stats (condensed)
  - fixed bug involving playing dead and mg42s/shoving
  - fixed bug involving putteam and limited-life play
  - locked teams are now persistant across swap_team calls
  - added spec-locking functionality to command "lock"
  - added "lock all" and "unlock all"
  - greatly improved spec-locking
  - added pistol only and needle only modes
  - ammo capping changed significantly
  - modified display of life stats (newlines fixed)
  - medic hints can now be toggled
  - added ability to destroy own objectives
  - added ability to disable objectives/objective spawning
  - added cvars for med/engr grenades and clips
  - added cvar for fatigue recharge rate
  - added warmup announcement
  - fixed gib logging reversal
  - fixed bug involving giveammo
  - added launcha/throwa/flinga/slapa
  - removed unlag
  - removed private messaging (replaced w/name-matching PMing)
  - removed g_allow* cvars (replaced with g_max* cvars)
  - fixed various wolf exploits

Beta 2.07-1.4 - released 11/17/02
  - removed g_ammoGivesPistol

Beta 2.07 - released 8/26/02
  - added corpse dragging
  - added proximity mines
  - added unlag code
  - added ammo giving/donating
  - added warmup damage
  - added group/name-matching private messaging
  - added uncap ammo option
  - added ignore feature (console and vote)
  - added warmup freeze
  - added complaint form ignoring
  - added playing dead
  - added ammo hints (display how much ammo teammates have)
  - added client num finding feature
  - added console slap player
  - added console kill player
  - added console force team
  - added console lock/unlock teams
  - added console force respawn
  - added console print commands
  - added console orient/disorient/shake player
  - vote strings are displayed in console
  - shuffle now keeps previous round and timeleft values for stopwatch mode
  - team gib messages are now yellow

Beta 2.06.1 - released 8/14/02
  - fixed bug involving scoring simultaneous self and other player gibs
  - fixed bug involving scripted map explosions and property damage
  - fixed bug involving fast-loading clients being allowed to late join when
    g_allowLateJoin was off
  - gib messages in client-side log now end names with ^7

Beta 2.06 - released 8/6/02
  - g_allowPF now defaults to 1
  - added chat filter/censor feature
  - manual spectator voice chats are disabled unless g_allowSpecChat is on
  - added special soldier weapons limiting feature
  - added private messaging feature
  - added team shuffling feature
  - added auto team shuffler (shuffle after team has x more wins than the other)
  - auto shuffler now takes into account stopwatch mode
  - added clan alignment feature
  - added headshot mode
  - disallowed dropping weapons while frozen
  - added "unstick" fix to freeze tag
  - fixed bug involving "most kills/revives" stat display
  - added disallowed votes feature
  - added votes for freezetag and headshot modes
  - fixed bug involving g_allowStartVotes
  - medics can revive underwater through g_waterRevive
  - flamers can damage themselves in ff-off mode through g_flameDamage
  - added censored word penalty (gibs players who say a censored word)
  - freeze tag unfreeze timeouts now respawn players at spawn points
  - added more help messages to freeze tag mode

Beta 2.05 - released 7/23/02
  - g_allowLateJoins now defaults to 1
  - g_flagPoints now defaults to 1
  - fixed "getting stuck in something after being revived" bug
  - added freeze tag gametype
  - added property damage feature
  - shortened length of /classes printout
  - spectator stats no longer logged
  - fixed minor bug involving axis/allies left being printed during intermission
  - added rotating server messages/MOTDs
  - added "FF off" mode to touch gibbing
  - added most kills/revives to end of round stats
  - dynamite warnings are now adjustable
  - start round ammo penalties now adjustable
  - added recognition for blocking an air strike
  - ammo packs can give helmets back
  - launch/throw/fling no longer have 3-space limitation
  - allow binoculars for non-LTs
  - added spawnhats to fun mode
  - MG42 repairs and blowing objectives is now logged

  - changes made to freeze tag since its initial beta:
    - changed from fire to green icon over head when frozen
    - added console messages with method of death
    - added logging of freezes and unfreezes
    - added freeze tag stats and most freezes/unfreezes
    - added needle proximity unfreezing
    - fixed bug involving objectives
    - added Medic! cry to freezes
    - added point system
    - added "Axis Win/Allies Win" banner to end game check
    - fixed bug involving end game checks
    - rewrote unfreeze code so that g_unFreezeTime is no longer relative to
      server load
    - added g_freezeRespawn to set a time-out for automatic unfreezing
    - added g_unFreezeInvul to set invulnerability time after unfreezing
    - disallowed frozen from picking up items (e.g. dropped objective)
    - frozen now drop to near or at ground level
    - fixed quirk involving flamethrower (client-side flame chunks)

Beta 2.04.2 - released 7/14/02
  - g_shove now defaults to 1
  - g_legDamage now defaults to 0

Beta 2.04.1 - released 7/13/02
  - changed way in which kill ratios are displayed
  - removed sarcastic remarks when player makes zero kills during life

Beta 2.04 - released 7/12/02
  - g_showTapOut now defaults to 0
  - renamed launch/throw/fling to launchn/thrown/flingn
  - added new launch/throw/fling that use clients' names
  - added g_allowLTF to disable launch/throw/fling and fun mode entirely
  - added friendly dynamite 5-second reminder g_dynaWarn
  - added g_medObjHeal to disallow medics self-healing when carrying objectives
  - enabled black text in names
  - added times killed and kill ratio to stats
  - silenced Medic and Need Ammo voice chats for fun mode
  - increased grenade and PF ammo given in fun mode
  - added round start ammo request penalty g_startAmmoPenalty
  - added leg shot speed reduction through g_legDamage
  - players can switch teams during warmup, regardless of g_maxlives

Beta 2.03 - released 7/06/02
  - added LT ammo distribution prioritizer
  - added no LT reminder
  - added g_lifeStats to toggle limbo displaying stats during life or round
  - added g_lockSpecs to keep spectators static and pointed up
  - added back team-switch anti-spam code with 5 second delay instead of 30
  - added g_flagWins to remove checkpoint flags
  - added location of downed player to medic reminders
  - added g_LTShootPenaltyRange to allow customization of enemy detection
  - increased default range of enemy detection for LT Shoot Penalty
  - fixed bug involving medic reminders detecting spectating medics
  - fixed bug involving last kill of the game not being counted in stats
  - added a number of cVars to customize bonus point values
  - added g_allowVenom to allow/disallow venom guns
  - added g_allowFlamer to allow/disallow flamethrowers
  - added g_allowMauser to allow/disallow mausers
  - helmet protection is now customizable (g_helmetProtects, g_helmetDamage)
  - fixed bug where late joiners in limited life games would still spawn
  - added g_showRevives to print revives in console
  - a player is no longer late join killed if only player on server
  - added g_teamCountConsole to enable/disable team counts in console
  - gib points can now be toggled with g_gibPoints
  - gib reporting can now be toggled with g_reportGibs
  - added g_logStats to print stats in server log at end of round
  - added g_showTapOut to disable "tapped out into limbo" messages

Beta 2.02 - released 7/02/02
  - added medic reminders
  - LT damage return now disabled when enemy is nearby
  - changed touch-gibbing to use id's new g_knifeonly code to remove weapons
  - spectators can talk to non-specs (cvar toggled, default off)
  - fixed minor spacing issue in the "tapped out" limbo message
  - ammo packs now add pistol ammo (cvar toggled)
  - changed how late joins are logged
  - now tracking team kills, gibs, team gibs, ammo & health given, head shots
  - print stats in console at end of round for each player
  - added cvar to log stats
  - Axis/Allies left reporting now cvar toggled
  - Axis/Allies left decreased frequency of reminders
  - launch/throw no longer work when someone is dead or spectating
  - added server-side command "fling"
  - added client-side command "stats"
  - added server cvar to disable corpse sinking
  - fixed bug where specs' stats would be displayed to someone else
  - fixed bug involving specs switching who they're following
  - fixed bug involving late joiners respawning in OLTL games
  - added distance function to debug mode
  - added range function to debug mode
  - added no medic reminder
  - added client-side command "classes"

Beta 2.01 - released 6/30/02
  - suicides (/kill) are explicitly reported in server log as such
  - fixed bug where self-initiate limbos would be counted as gibs by whoever
    last killed you
  - fixed bug where those joining during warmup would be late-join killed
  - decreased spawnguns to 1-10 guns (was 1-25)
  - decreased spawnnades to 10 nades (was 40)
  - added headshot detection to debug mode (debug mode is undocumented)
  - allow ammo pack syringes to be enabled/disabled by a server cvar

Beta 2.0 - released 6/29/02
  - Updated for RTCW 1.33 code
  - Added grenade shower
  - Inactivity drop won't apply to fallen
  - Added give grenades on demand for g_fun mode
  - Fixed bug with giving health on demand (could revive self)
  - /spawnGun renamed to /spawnGuns
  - Fixed spawnGuns/spawnNades spam exploit (server could crash when it exceeds
    max entities)
  - Added global debug cvar
  - Added touch-gib mode

* Changes prior to 2.0 were not documented

================================================================================
2. INSTALLATION
================================================================================
This is a 1.4/1.41-compatible mod.  If you are not running 1.4/1.41, you *will*
experience Weird Things during gameplay.

Windows:
  - Create a folder "shrubmod" inside your Return to Castle Wolfenstein Folder
    e.g. C:\Program Files\Return to Castle Wolfenstein\shrubmod
  - Extract "qagame_mp_x86.dll" into this folder
  - Copy your server.cfg or create one in this folder
  - Run RTCW with the flag "+set fs_game shrubmod"
    (create a shortcut to RTCW, right click on it and select "Properties", and
     in the "Target" box, add " +set fs_game shrubmod" at the end)

Linux:
  - Create a directory "shrubmod" in your RTCW directory
    e.g. "/usr/local/games/wolfenstein/shrubmod"
  - Extract qagamei386.so into this directory
  - Copy your server.cfg or create one in this directory
  - Run RTCW with the flag "+set fs_game shrubmod"

In both cases, copy the soundpack.pk3 file to /main if you would like to use
that.

If these directions aren't clear, you probably shouldn't be running a server!

================================================================================
3. ACKNOWLEDGEMENTS (In alphabetical order)
================================================================================
The following people helped in testing and/or contributed ideas or bug reports:
  2.01 and prior: D6M14, EvilOne, Glocksmith, Leviathan, Liquidity, logosmani,
    Mr. Underhill, Phreylan, pure
  2.02: Bishop, BluNereid, Dima
  2.03: Bishop, Glocksmith, HappyG, Infinite, KingFlea, PizDets, Reflexion
  2.04: Reflexion, Scott307
  2.05: BlackRider, CorbinDallas, d-dog, ^Evil^, Glocksmith, Viagra
  2.06: Artemis, BlackRider, Daz, Dead Penguin Clan, Glocksmith, HappyG,
    Jimbonics, Shredded Wheat
  2.06.1: LANMAN, Reflexion
  2.07: 420, Adrian, EvilOne, HappyG, LANMAN, logosmani, Phreylan
  3.0: 420, BlooDHounD, Cpl. Konig, DaBadGuy, Glocksmith, Knuckles, LANMAN,
    Lehmann, Mad Gasser, MMmmGood, Otto, RedHo7Lea7her, Toter, Vomitron
  3.0.1: SuperGreg
  3.1: Artemis, Ben, Eyes, KarenSue, LANMAN, MMmmGood, Otto, Resolution 208,
    SolidSnake

It's likely that I forgot to mention some people for 3.1 - I'm ramming through
this documentation and it's 4 AM now.  You know who you are, and I thank you.

Thanks to Bocce, Cpl Konig, Robob, and Surgeon for their support.

Thank you to chmod 700 for his contributions to this project.  Not anymore,
though!

Thank you to The Quark, who originally ran his own mod for 1.31 featuring gib
reporting/points, axis/allies left, and late join killing.

Thanks to Neil Toronto for his Unlagged server-side lag compensation code, used
in 2.07, removed in 3.0, and back again in 3.1.

================================================================================
4. COMMAND SUMMARY
================================================================================

Removed.

================================================================================
5. WHAT'S NEW/MODIFIED FOR 3.1
================================================================================

CUSTOMIZABLE GAME VARIABLES/FEATURES NOT NEEDING MUCH EXPLANATION
--------------------------------------------------------------------------------
Cvars:		     Default	Description
  g_trimLog             0       Omit ClientConnect/item lines from logfile
  g_ROF                 1       Proportional to delay between MP40/Thompson
                                shots (e.g. 0.7 is fast, 2 is twice as slow)
                                Potential bandwidth hog!
  g_showVotes           0       Display how each player votes

Commands (client-side):
  cpclient <clientnum> <text>	Someone asked for it.


ACCURATE / OVERHEATING MG42s - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_MG42	default: 1	enable/disable enhanced MG42s

When g_MG42 is 1, MG42s are now very accurate for the first 6-8 seconds of use.
Afterwards, accuracy will deteriorate rapidly until overheating at 10-12
seconds.  After overheating, the MG42 cannot be used until it has cooled.  An
engineer can facilitate cooling with his pliers.


AIR STRIKE BLOCKING - new for 2.05
--------------------------------------------------------------------------------
  g_ASBlock	default: 0

When g_ASBlock is on (1), players are recognized in the center of the screen
when they block an air strike.

New for 3.1: g_ASBlock is now a bitflagged variable with the following features:

1 - same as before - announce air strike blocks
2 - relax the air strike blocking detection so that air strikes are more easily
    blocked on flat surfaces
4 - play a sound to the player who blocked an air strike (so that he knows when
    to bail) - may not always transmit

Add up the desired values to set g_ASBlock.


CROUCHING ENHANCSE STAMINA RECHARGE RATE - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_crouchRate  default: 5      proportional to recharge rate when crouched

g_crouchRate is roughly proportional to the stamina recharge rate when
crouched.  At the default of 5, the stamina recharges at a noticably quicker
rate when crouching.  It is 3 in the morning and this is as creative as I can
get with this description.


CUSTOMIZABLE OBJETIVE SPAWNING - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_noObjSpawns		0

g_noObjSpawns is now bitflagged and controls exactly which objectives will not
be spawned, allowing it to be used on maps like Depot and Assault.  Trial and
error will reveal what the right values you need are.  Naturally, setting it to
-1 will not spawn any objectives.

Because I'm feeling generous, here are some values:

Castle:
 1 - Coffin		2 - Hallway Gate	4 - West Gate
Depot
 1 - Axis Fwd Deploy	2 - Allied Fwd Deploy	4 - Anti-Aircraft Gun
 8 - Allied Field Ops
Assault
 1 - Warehouse Door	2 - Gate Hatch		4 - Comm Tower
The Damned
 1 - Main Water Regulator (NO)			2 - Right Door
 4 - Main Dam Road Doors			8 - Left Door
 16 - Lower Dam Tunnel Door			32 - Upper Dam Stairwell Door

Remember that this sets objectives that will NOT spawn.


DISGUISES - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_disguise   default: 0

When g_disguise is enabled, certain players can disguise themselves in an
enemy's uniform by finding a dead enemy who has not yet gone into limbo, walking
up to him, and pressing the +activate key while pointing at him (the same key
used to open doors).

If successful, the enemy will enter limbo and the player will now be disguised
and told so in the center of his screen.  A player can verify that he is
disguised by switching weapons, which will print a message indicating if he
is disguised.  A player loses his disguise if he:

 1) Takes a flag or objective
 2) Uses a weapon other than the knife
 3) Dies

Note that when a player loses his disguise by firing his weapon, the initial
shot will not be sent to other players, but might be seen by the disguised
player because of latency.

Disguised players will appear without a name/health bar to enemies.

g_disguise is a bitflagged variable that controls who can be disguised:

1 - only soldiers with SMGs can disguise
2 - non-soldiers can disguise (heavy weapons not necessarily allowed)
4 - heavy weapons are allowed

Add the desired values to set g_disguise.

If 4 is not set, then a disguised player who drops his weapon and picks up
a Panzerfaust, Flamethrower, Mauser, or Venom will lose his disguise.

This feature has not been fully developed and there have been instances
in which heads were not rendered.  Though it does not lead to server instability,
it is either deeply unsettling or hilarious.


HIGH ACHIEVERS ANNOUNCEMENTS - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_highAchievers	default: 4095

When g_highAchievers is set, the players with the greatest stats for the
previous round will be announced in the center of the screen during warmup.  If
two players share the same title, no player will be announced for that category.

g_highAchievers can be set by adding up the desired values:

1 - Most Kills
2 - Most Deaths
4 - Most Gibs
8 - Most Revives
16 - Greatest Accuracy
32 - Most Property Damage
64 - Most Binoculars Collected
128 - Most Health Given
256 - Most Ammo Given
512 - Most Poison Kills
1024 - Most Goomba Kills

For example, if you want to display the most kills and most revives during
warmup, set g_highAchievers to 9 (1+8).  Note that in order to be considered for
the accuracy stat, a player must have shot 30 bullets or more during the round.

Two additional options can be set:

2048 - Auto Adjust Warmup
4096 - Print in the Console

Auto Adjust Warmup will automatically set warmup to the amount of time required
to accomodate the high achievers list.  If you do not have Auto Adjust Warmup
enabled, you must set warmup to at least 7 seconds + 4 seconds for each high
achiever displayed.  Print in Console will print the high achievers in the
console as they are displayed in the center of the screen so that they can be
referred to later.

The default setting, 4095, corresponds to everything but Print in the Console.

New for 3.1: g_highAchieversMin specifies the minimum amount of time, in seconds,
that the warmup should be set to when Auto Adjust Warmup is enabled.  By default,
it is 0.  This is useful for setting a long minimum time during map changes.


HIT SOUNDS - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_hitSounds	        default: 0      enable/disable hitsounds

Commands (client-side):
  hitsound

When g_hitSounds is enabled (1), players can type "/hitsound" in the console to
hear a "hit" sound play when they hit another player.  It's a hitsound.  It's
also a potential bandwidth hog.  This requires the soundpack to be installed on
both the client and server.


IMPORTANCE FLAG - new for 3.0
--------------------------------------------------------------------------------
Commands (server-side):
  important <clientnum>, unimportant <clientnum>

The server-side command "important <clientnum>" will set that client as
important.  A user flagged as important has the following privileges:

  1) Cannot be vote kicked or ignored
  2) Can switch teams at any time, regardless of maxlives or balance
  3) Can bypass chat censorship
  4) Can bypass chat and voicechat flood protection
  5) Can see all team chat as a spectator
  6) Can vote and call votes as a spectator
  7) Can chat as a spectator regardless of g_allowSpecChat
  8) Cannot be filed against
  9) Can drop objectives an unlimited amount of times
 10) Can play any lsound

This is intended for use in an automated form, e.g. a bot.  "unimportant
<clientnum>" will revert "important".

New for 3.1: Importance can now be specified more explicitly with the
addition of an extra parameter when using "important":

  important <clientnum> <importanceLevel>

These levels include (add the desired values):
  1     - can chat as a spec and see team chat as a spec
  2     - can bypass voice/chat flood protection
  4     - can bypass censoring
  8     - can change teams at will
  16    - can vote/call a vote as a spectator and cast multiple votes
  32    - cannot be vote kicked or vote ignored
  64    - cannot be filed against for a TK
  128   - cannot be shoved
  256   - can drop objectives unlimited times
  512   - can throw unlimited smoke grenades
  1024  - can play any sound
  default: 2047 (if no number specified)


KICKABLE AIR STRIKES - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_kickable    default: 0

When g_kickable is 1, air strikes can be kicked by pointing at them from up
close and using +salute on it.  LTs can be very evil by throwing an airstrike
inside, near a doorway, and kicking it outside at the last moment.


LIEUTENANT MODS - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_lieutenant  default: 0

g_lieutenant allows you to increase the number of ammo/grenade packs that can
be thrown per full charge as well as zero out the charge bar when an LT spawns.

These options are useful for large servers prone to air strike "spamming."

1 - ammo packs take 1/8 charge bar
2 - ammo packs take 1/16 charge bar
4 - LTs start with 0 energy in charge bar
8 - grenade packs take 1/2 charge bar
16 - grenade packs take 1/4 charge bar

Add the desired values to set g_lieutenant.  Naturally, 1 & 2 and 8 & 16 should
not be added simultaneously.  The "ammo given" stat is scaled appropriately for
8 and 16.


LOGFILE OUTPUT CHANGES - new for 3.1
--------------------------------------------------------------------------------
The logfile output has been changed considerably.  I don't have time to cull all
the documentation I did for it as most admins will have no need for it.  Email
me if you really need it and want to learn about other stuff you might like, but
the most obvious things are easy to figure out just by being familiar with old
output.  This'll get you started:

Changed the following MODs to:
  poisoning => MOD_POISON
  shoving => MOD_SHOVE
  goomba => MOD_GOOMBA
  MOD_SLIME => MOD_DYNAMITE
  MOD_GRAPPLE => MOD_CRUSH
  MOD_POISONGAS => MOD_FALLING
  MOD_ZOMBIESPIT => MOD_SUICIDE
  MOD_ZOMBIESPIRIT => MOD_TRIGGER_HURT
Added the following two (they replace MOD_LAVA)
  MOD_ARTILLERY, MOD_AIRSTRIKE


POLL VOTE - new for 3.1
--------------------------------------------------------------------------------
Commands (client-side):
  callvote poll <question>

A user can cause the contents of <question> to be voted on - no more using 
"callvote map <question>" :).  Note that "poll" is included in g_disallowedVotes
by default and must be removed from that cvar in order to be used.


PROXIMITY MINES - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_mines		default: 0	enable/disable proximity mines
  g_mineActivate	default: 3	seconds before an armed mine activates
  g_mineAutoDisarm	default: 1	disarm mine when player limbos
  g_mineShootable	default: 0	whether mine can be shot at or not
  g_mineHints		default: 1	enable/disable mine hints
  g_outdoorMines	default: 0	enable/disable outdoor-only mines

Commands (client-side):
  mine, usemine

When g_mines is on (1), engineers can plant proximity mines in the same manner
that they plant dynamite.  In order to set their next dynamite to become a mine,
engineers must issue the command /mine or /usemine (it is recommended that these
commands be bound to a key).  They will receive confirmation on their screen
that mines are enabled, and they can switch back to dynamite if they change
their mind by issuing the command again.

After planting a mine, they have g_mineActivate seconds to move away from the
mine before it activates.  When the mine is activated, it strobes red and is
easily distinguishable from regular dynamite.  When a player enters the red
light radius and has sufficient line-of-sight with the mine, the mine will
detonate.

Mines can be destroyed using 2-3 grenades or other explosives.  By default,
mines can only take damage from explosives, but if g_mineShootable is on (1),
mines can take damage from other weapons.  It is recommended that
g_mineShootable remain at its default.

Engineers are limited to one mine at a time.  By default, their mine will
deactivate and sink into the ground if they die and enter limbo.  If you
prefer to have the mine persist across multiple respawns, set
g_mineAutoDisarm to 0.

New for 3.0: When g_mines is 2, only enemies can trigger mines.

When a user has dynamic lights on, he can see that axis mines are yellow and
allied mines are violet.

g_mineHints 1 will enable notifications when one is near a friendly mine, with
text depending on the status of g_mines.

Clients can also use the command "alt" while holding dynamite to enable/disable
a mine.  Suggestion: bind your weapalt key (for the mauser) to "weapalt; alt".

Setting g_outdoorMines to 1 will permit mines to only be planted outside.

New for 3.1:  Setting g_mineWalk to 1 will allow players to crouch through
a proximity mine.  Setting it to 2 will allow players to walk or crouch
throw proximity mines.


PUTSPECS VOTE - new for 3.1
--------------------------------------------------------------------------------
Commands (client-side):
  callvote putspecs <clientnum/partial name>

A user can call a vote to move a player to the spectator team with the client
number or partial name as a parameter.  This is useful for limited-lives or one-
life games.  Note that "putspecs" is included in g_disallowedVotes by default
and must be removbed from that cvar in order for this vote to be usable.


PANZERFAUST ARC / PARTY PANZER - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_panzerfaust		default: 0

Setting g_panzerfaust to 1 will give the Panzerfaust a realistic arc, allowing
gravity to pull down the rocket as it travels.

Setting g_panzerfaust to 2 will turn the Panzerfaust into a Party Panzer that
spits out ammo, health, and grenade packs and does no damage.


SOUND PACKS - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_allowedSounds
    default: "fear1 fear2 halt heifer meingut schnell scream smellhim stophim"

Commands (client-side):
  lsound <sound> [text]

Commands (server-side):
  gsound <sound>

Sound packs are now supported in the shrub mod.  The included soundpack.pk3 file
contains a selection of sounds that you can use with this feature.  If you would
like to use it, you must copy the soundpack.pk3 (do not unzip it!) to your /main
folder, not the /shrubmod folder.  Be sure to restart the server for the sounds
to be loaded.

Clients will not hear a sound unless they have a copy of it on their machine.
Thus, you should make soundpack.pk3 available for download and instruct your
players to copy it to their /main folder.  Players will still be able to play
as normal if they do not have sounds that are on the server.  Naturally, they
would not hear them.

There are two commands used to play sounds:  gsound and lsound.  Gsound, or
global sound, can only be used through the console/rcon.  The syntax is:
gsound <sound>

Sound is the name of the sound, without the .wav extension.  If you open up the
soundpack.pk3 file in a program that can read zip files, you will see some of
the sounds available for your use.  For example, you can type "gsound heifer" to
play heifer.wav.  There are also sounds in 300+ MB pak0.pk3 file, and mp_pak
files that you can use.  Note that you must specify the paths in these files.
For example, "rcon gsound sound/beast/skull_shriek1" (this is in pak0.pk3).
Notice that the sounds in soundpack.pk3 are not contained in subfolders, so
no path is required.

Clients can play sounds that are specified in g_allowedSounds using the lsound
command.  Lsound, or local sounds, are heard by the client and those in his
vicinity.  The syntax is similar to gsound.  For example, "lsound fear1".  Any
text after the name of the sound is printed in the chat window, giving the
effect of a voice chat.  For example, "lsound fear1 Don't kill me!".

If the sound specified after "lsound" is not in g_allowedSounds, it will not
play.  If a player is flagged as important, he can play any sound, regardless of
its presence in g_allowedSounds.

Creating your own sound pack to add new sounds and voice chats to the game is
relatively straightforward.  Simply collect the sounds you wish to add and
convert them to mono if necessary.  You *must* save them in PCM format with no
compression.  Add them to a .zip file and rename the file to .pk3.  Copy this
new .pk3 file to /main and allow your players to download and copy it to their
/main folders, as well.  DO NOT ASK ME FOR HELP WITH THIS PROCESS.  I WILL *NOT*
RESPOND.  USE TRIAL AND ERROR IF YOU DON'T GET IT RIGHT THE FIRST TIME.

Installing the included soundpack.pk3 will play a sound of a body collapsing
when players die and screaming sounds when players are first hit with a
flamethrower.

New for 3.1:  The following additions were made to sound packs:

Cvars:
  g_sounds      default: 1     enable/disable soundpack commands

Commands (client-side):
  tsound <sound> [text]
  asound <sound> [text]

g_sounds is a bitflagged variable that enables the soundpack commands like so:

 1 - lsound
 2 - tsound
 4 - asound

Lsound is the same as in 3.0 (see above).  Tsound broadcasts to the player's
team.  Asound broadcasts to all players.  Add up the desired values.

Also new for 3.1:  Setting g_allowedSounds to "*" permits any sound to be
played.


STATS CHART - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_stats	default: 1

g_stats is a bitflagged variable that controls stats displays in the console
during the end of the round.  Add up the desired options to set g_stats:

1 - standard end of round stats that existed in previous versions
2 - new, expanded stats table that displays stats for every player

Type "/? stats" in the console to view an explanation of the column headers.

Note that I am aware that two-digit kill ratios break the alignment of the table.
This will be fixed.


TEXT SHORTCUTS - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_shortcuts	default: 0	enable/disable text shortcuts
When g_shortcuts is 1, players can use the following text shortcuts in their say,
say_team, and me chats.  They will be replaced with the appropriate text:

 [a] - the last teammate who gave you ammo
 [d] - the last player who killed you
 [h] - the last teammate who gave you health
 [k] - the last player who you killed
 [l] - your location
 [p] - the teammate you are pointing at
 [r] - the last player who revived you
 [n] - your name
 [s] - the amount of health you have
 [w] - the weapon you are holding

For example, one can "bind n say_team I need help!  I'm at [l] and I only have
 [s] health left!" or "bind l say_team [p], turn around!  I'm trying to give you
a [w]!" ([w] could read "Ammo Pack" or "Health Pack" depending on what a player
is holding, or it could be left out).

Helpful hint: type "/? shortcuts" in the console to see the above list in-game.


TRIP MINES - new for 3.1
--------------------------------------------------------------------------------
Cvars:
  g_tripMines   	default: 0
  g_tripMineLength	default: 320    max length of trip mine wire

When g_tripMines is 1, Engineers can plant trip mines through the following
procedure.

 * You must be holding your pliers.
 * Type /tm or /tripmine.  A dynamite pops out.  Arm it.
 * Walk to a nearby wall/surface, point at it, and use /tm or /tripmine on it.

If you did it correctly, a wire will extend from the dynamite to the "anchor
point" that you specified.  Yes, there is a tired, old AYB reference there which
mildly amused some during testing and was probably meant to have been removed
but remains since it's not a big priority right now.

This process also works with "alt", and I highly recommend using "alt" instead
since it works with smoke grenades, grenade packs, and mines too.

g_tripMineLength can be set to 0 to disable the length limitation.  This is fun
on maps like Dam and Tram.  Setting g_tripMines to 2 will permit teammates to
walk through the wire without setting it off.

The trip mine will deactivate if the Engineer who planted it goes into limbo.

You can also deactivate a trip mine immediately using /tm or /tripmine (or /alt
if holding the pliers).

Helpful hint: tell your players to type "/? tripmines" and instructions will
be sent to their console.


WARMUP DAMAGE - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_warmupDamage	default: 0	enable/disable warmup damage

When g_warmupDamage is on (1), players can damage and kill other players and
game objects during warmup.  Kills, gibs, and revives are not logged.

New for 3.1: g_warmupDamage is not bitflagged with two new options in addition
to "1" for activating warmup damage.  Add "2" to prevent anyone from taking
damage unless he or she has used the +attack button.  Add "4" to allow for
instant respawning once a player goes into limbo during warmup.  For example, if
you want to enable all options, adding 1, 2, and 4 means you would set
g_warmupDamage to 7.


================================================================================
6. WHAT'S NEW/MODIFIED FOR 3.0
================================================================================

CUSTOMIZABLE GAME VARIABLES/FEATURES NOT NEEDING MUCH EXPLANATION
--------------------------------------------------------------------------------
Cvars:		     Default	Description
  g_medicNades		1	# of nades medics begin with
  g_LTNades		1	# of nades LTs begin with
  g_medicClips		0	# of spare clips medics begin with
  g_engineerClips	1	# of spare clips engineers begin with
  g_spawnInvul		3	spawn invulnerability time
  g_reviveInvul		3	revive invulnerability time
  g_staminaRate		1	proportional to stamina recharge rate
  g_destroyOwnObj	0	allow defender to destroy objectives
  g_corpseWar		0	requires map_restart to reset :)
  g_maxClips		3	max # of clips one can store in reserves
  g_54321		1	enable/disable warmup voice countdown
  g_waterSniping	1	allow/disallow water sniping *
  g_packDistance	1	proportional to distance med/ammo packs travel
  g_medicHints		1	enable/disable "You are closest med..." messages
  g_complaintForms	1	enable/disable TK complaint forms (for bots)
  g_pistolOnly		0	enable/disable pistol only round (map_restart)
  g_needleOnly		0	enable/disable needle only round (map_restart)

* If you don't know what this is, leave it alone.  As a side effect, this will
disable sniping while in shallow water.


CLASSES
--------------------------------------------------------------------------------
Commands (client-side):
  classes

Upon issuing the "classes" command (either via bind or in the console using
/classes), a player will receive, in the center of his/her screen, the number of
each class on his/her team.

New for 2.05:  The length of the printout has been shortened to prevent line
breaks on large servers.

New for 3.0: Team counts are now displayed in "classes".  Classes will also
be displayed in the chat window area of the high achievers list is active.


CLIP DROPPING - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_dropClips		default: 1	enable/disable clip dropping

When g_dropClips is on, extra clips of ammo in a player's reserves are dropped
along with the ammo already in the gun.  A player who picks up the gun will
receive the extra ammo.


DISABLE OBJECTIVE SPAWNING - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_noObjSpawns		default: 0	enable/disable objective spawning

When g_noObjSpawns is on, any objective that requires dynamite will not appear
when the round begins.  This is intended for special circumstances such as
playing The Damned or Chateau in an OLTL setting.  If this feature is enabled
on a map such as Sub or Depot, the round will end prematurely.  Make sure you
set this appropriately in the rotation if you plan on using it.


DISALLOW LATE JOINS
--------------------------------------------------------------------------------
Cvars:
  g_allowLateJoins	default: 1	allow/disallow late joins
  g_lateJoinTime	default: 15	time after round start when one is late

If g_allowLateJoins is off (0), any players joining the game after
g_lateJoinTime seconds will be killed.  This method is preferrable to blocking
joins using other methods since it allows players to pick their team immediately,
without affecting gameplay.  Late joins are logged in the following format:

Late Joiner: <clientNum>: <clientName>

New for 3.0: When g_allowLateJoins is 2, late joiners are given a number of
lives proportional to the time they joined the game.  For example, if the
timelimit is 20 minutes, maxlives is 30, and a player joins 10 minutes into
the game, he will receive 15 lives.  This works with the axis/allied maxlives
cvars, as well.


FRIENDLY-FIRE RETURN DAMAGE - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_friendlyFire

When g_friendlyFire is set to 2, damage given to teammates is returned to the
damager.


GOOMBA / DEATH FROM ABOVE - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_goomba		default: 1	enable/disable disable goomba killing

When g_goomba is enabled (1), players can jump on top of enemies to inflict
damage.  The higher the jump, the more damage is given.  If the height is
sufficient, a jump can kill an enemy.  Increasing g_goomba scales the damage
done by that amount (decimals are okay).  Jumping on an enemy will also break
your fall.  Jumping on a teammate will not damage the teammate, but it will
break your fall.


GRENADE PACKS - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_grenadePacks	default: 1	enable/disable LT grenade packs

Commands (client-side):
  grenades, gp, alt

When g_grenadePacks is on (1), Lieutenants can toggle grenade packs on/off by
using the command "grenades".  The procedure is similar to mines.  After
enabling grenade packs, the next ammo pack thrown will be a grenade pack which
contains 4 grenades.  Grenade packs require a full energy bar.

Clients can also use the command "alt" while holding an ammo pack to enable/
disable a grenade pack.  Suggestion: bind your weapalt key (for the mauser) to
"weapalt; alt".


GROUP / NAME-MATCHING PRIVATE MESSAGING - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_privateMessages	default: 1	turn on/off private messages
  g_minMsgChars		default: 2	min number of characters in name

Commands (client-side):
  mg, pm, m

These commands differ from the regular private messaging commands in that they
do not require a complete name to match.  The syntax is:

/m <match> <message here>

The same syntax applies to /pmg and /mg.  The message is sent to every player
who contains <match> in their name.  This can be one match or multiple matches.
This is particularly useful for messaging an entire clan.  This command is
more versatile than the existing private messaging command and will eventually
replace the existing private messaging commands.

The <match> string must be g_minMsgChars in length or greater to prevent abuse.

New for 3.0:  Group/name-matching private messaging now uses the commands "msg",
"pm", and "m".  Messages are now displayed in the chat window if they will fit.
Otherwise, they are displayed in the console with a reminder in the chat window.


LAUNCH/THROW/FLING
--------------------------------------------------------------------------------
Cvars:
  g_allowLTF		default: 1	enable/disable launch/throw/fling

Commands (server-side):
  launch <clientname>		launch <clientnumber>		launcha
  throw <clientname>		throw <clientnumber>		throwa
  fling <clientname>		fling <clientnumber>		flinga

Launch propels a player into the air, while throw pushes them forward and
slightly into the air.  This is intented to be used in conjunction with programs
such as Wolfenstein Admin Bot which can parse "say" or "say_team" commands for
"launch" and "throw" and a player name and send the appropriate command along
with the client number derived from the specified player name.  However, there's
nothing stopping you from experimenting with the command manually.  e.g., if
you're the first person to connect, a "launch 0" will launch yourself.

New for 2.02: fling has the upwards velocity of launch and a random direction on
the x-y plane.

New for 2.04: launch/throw/fling have been renamed to launchn/thrown/flingn.
New launch/throw/fling commands have been implemented that will allow you to
use a client's name instead.  Do not use colors when typing the client's name.
You may have a maximum of three spaces in the name.  If there are more than
three spaces, use the client number instead.

g_allowLTF must be on (1) for any of the launch/throw/fling commands to work.
This variable can only be set to 0 through the command line (add the following:
"+set g_allowLTF 0").  It cannot be modified through the console.  This is
useful for admins who wish to deny launch/throw/fling to rcon users.

New for 2.05: The 3-space limitation no longer applies to launch/throw/fling.

New for 3.0: "launcha", "throwa", and "flinga" will launch, throw, and fling
all players on the server.


LIMBO DROPPING - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_dropBinocs		1	enable/disable binoculars dropping on limbo
  g_dropHealth		2	# of med packs medics drop when they limbo
  g_dropNades		2	# of nades engineers drop when they limbo

g_dropBinocs enables/disables dropping binoculars when players limbo, if they
have one.  g_dropHealth controls the number of medic packs a medic drops when he
limbos (0 disables).  g_dropNades controls the number of grenades an engineer
drops (unprimed :) when he limbos - it is the minimum of g_dropNades and the
number of grenades he has left in reserve (0 disables).  Dropped grenades will
add to a player's current grenade total and cap at the game default of 15.

The number of binoculars picked up are tracked and can be used with the high
achievers list.  Thus, even if you give every class binoculars, it may still be
fun to leave g_dropBinocs on.


LOCK / UNLOCK TEAMS - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  lock <team>
  unlock <team>

Locking a team will prevent further players from joining that team.  The team
specified can be either axis/red/r or allies/blue/b.  Unlocking it has the
opposite effect.

New for 3.0: added "lock specs" and "unlock specs", which will change
g_lockSpecs for you.  Additionally, you can now "lock all" and "unlock all"
to do the obvious.


OBJECTIVE DROPPING - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_dropObj		default: 3	# of times a player can drop objective

g_dropObj controls the number of times a player can drop an objective per life.
Objective dropping is achieved by holding the knife and hitting the +dropweapon
key.  Setting g_dropObj to 0 disables this feature.


MAP MESSAGES - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_mapMessages		default: 1	enable/disable map messages

When g_mapMessages is enabled (1), status messages pertaining to the map are
printed in the console along with the time they occured.  Examples of these
messages include "The Axis have recovered the forward deployment!", "The Allies
have stolen the documents!", and the like.

Duplicate messages are not printed.


MULTICLASS - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_multiClass		default: 0	enable/disable multiclass

When g_multiClass is enabled (1), every player will spawn with the special
abilities of the medic, engineer, and lieutenant.  Players can cycle through
the syringe/pliers/airstrike canister/ammo pack/health pack by repeatedly using
the weaponbank 5 and weaponbank 6 keys (typically bound to 5 and 6).  All
players have binoculars and can call artillery.  This feature is useful for 1v1
or 2v2 play.


"ON MY WAY" VOICE CHAT - new for 3.0
--------------------------------------------------------------------------------
Commands (client-side):
  omw

The command "omw" will issue the "Affirmative, on my way!" voice chat to the
user's team only.  This chat is subject to any flood protections.


POISON NEEDLES - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_poison		default: 20	damage per second/disable

Medics can use their needles against enemies to poison them by walking up to an
enemy and poking him with the syringe as he would a revive.  The poison will
disorient the enemy and deal g_poison damage per second until he picks up a
health pack or dies.  Setting g_poison to 0 disables this feature.


PRINT COMMANDS - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  chat <message>
  chatclient <clientnum> <message>
  cp <message>
  print <message>

Chat, cp, and print write text to the chat area, center of the screen, and
console, respectively.  Text from these commands is not preceeded by "Console:",
and you may use \n to add a line break for chat and print.

New for 3.0: chatclient can be used to send chat area text to a specific player.
These 4 commands are now also logged.


RCON LITE - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_rconlPassword	default: blank	RCON Lite password
  g_rconlCommands	default: blank	RCON Lite allowed commands

Commands (client-side):
  rconlpassword <password>, rconl [password] <command>

When g_rconlPassword is set, users with this password can run commands specified
in g_rconlCommands.  For example, suppose you have the following in your config:

seta g_rconlPassword "asdf"
seta g_rconlCommands "map_restart clientkick g_gravity"

A player can then use the command "rconl asdf g_gravity 800".  Similarly, a
player can avoid having to type the rconl password every time he issues an rconl
command by using "rconlpassword asdf" first.  After setting rconlpassword, the
rconl command can simply be "rconl g_gravity 800".

This is useful for allowing certain people to administrate the server without
granting them full access.  Note that responses from the server will not be
displayed to the rconl user (e.g. status), but client numbers can still be
found using "clientnum".


READY-UP SYSTEM - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_readyUp		default: 0	enable/disable ready-up system

Commands (client-side):
  ready, notready

When g_readyUp is 1, warmups will be indefinately long until every Axis or
Allied player has indicated he is ready by issuing the command "ready".  Though
the warmup will still countdown, it will revert to 1000 when it reaches 10.
The names of players not ready will cycle in the center of the screen until
every player is ready.  When all players have readied-up, the warmup will 
reset to 10 seconds and the round will begin after that time has expired.


REPEAT FLOOD PROTECTION - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_maxRepeats		default: 2	number of repeats allowed

g_maxRepeats defines the maximum number of times an identical string of text can
be said before it is blocked from sending.  Setting g_maxRepeats to 0 disables
repeat protection.


SHOVING
--------------------------------------------------------------------------------
Cvars:
  g_shove		default: 1	turn shoving on/off
  g_shoveAmount		default: 80	amount of shove to apply
  g_shoveNoZ		default: 1	turn z-axis shoving on/off (1 means off)
  g_shoveOff		default: 0	enable/disable shoving team off ledges

Commands (client-side):
  +salute

When g_shove is on (1), players can shove each other using +salute.  For example,
players may "/bind mouse2 +salute" in the console.  Shoving will push the player
they are pointing at an amount proportional to g_shoveAmount.  Players can
crouch and look up to give some lift to their shove force, but only if
g_shoveNoZ is off (0).  This feature is useful for use against players who are
blocking doorways or other key areas.  You can also have fun with it by turning
g_shoveNoZ off (0) and increasing g_shoveAmount to something greater, e.g. 300.
I'd recommend playing catch: one player stands on another, the bottom player
shoves upwards and tries to catch the launched player.

New for 3.0: Shoving teammates to their death is now disabled by leaving
g_shoveOff at its default of 0.  This is particularly useful for Tram and Keep.
When g_shoveOff is 1, no anti-shove checks occur.


SLAPPING - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  slap <clientnumber>		slapa

This command will slap the specified player around for 2.5 seconds and deduct
approximately 25 health.  This command requires a clietnumber, which can be
found with the /clientnum helper function or used in conjunction with WAB.

This command requires g_allowLTF to be on.

New for 3.0: "slapa" will slap all players on the server.  It is not advisable
to use this on large (40+) servers.


SMOKE GRENADES - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_smokeGrenades	default: 15	smoke grenade time/disable
  g_smokeGrenadesLmt	default: 0	max smoke grenades per life

Commands (client-side):
  sg, smoke, alt

Lieutenants can toggle smoke grenades on/off with the command "sg" or "smoke"
(operation similar to mines).  After enabling a smoke grenade, the next air
strike can will become a smoke grenade, emitting smoke southerly for
g_smokeGrenades seconds.  Smoke grenades require 1/4 of the energy bar.
Setting g_smokeGrenades to 0 disables this feature.

Clients can also use the command "alt" while holding an airstrike canister to
enable/disable a smoke grenade.  Suggestion: bind your weapalt key (for the
mauser) to "weapalt; alt".

g_smokeGrenadesLmt sets the maximum number of smoke grenades that can be thrown
per life (from spawn to limbo).  Setting it to zero allows for unlimited smoke
grenades to be thrown.


SPECTATOR CLICKING - new for 3.0
--------------------------------------------------------------------------------
Free-floating spectators can aim and click on the person who they wish to
spectate.  If the point at which a spectator is aiming is not on a player, he
will spectate the next person that can be spectated.  Remember that the
+activate key will release a spectator from 1st-person spectating and back into
free-floating spectating.


SPECTATOR LOCKING - new for 2.03
--------------------------------------------------------------------------------
Cvars:
  g_lockSpecs		default: 0	turn on/off spectator locking

When g_lockSpecs is on (1), spectators cannot moved and are forced to look
straight up.  This is a simple mechanism to help curb any voice chat "cheating"
that may occur.  It's not perfect, however, as specs can still hear sounds.

New for 3.0: Spectator locking has been greatly improved, with nothing useful
visible or audible to the spectators.


THROWABLE KNIVES - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_throwableKnives	default: 1	initial number of throwable knives 
  g_maxKnives		default: 5	max knives one can carry
  g_knifeDamage		default: 35	max damage thrown knife gives

When g_throwableKnives is set, players start off with g_throwableKnives knives
that can be thrown by selecting the knife and hitting the "+dropweapon" key.
Setting g_throwableKnives to -1 gives players an unlimited number of knives to
throw.  Players can throw one knife every .75 seconds.  Setting
g_throwableKnives to 0 disables this feature.

If a player misses and the knife lands on the ground, it can be picked up and
added towards the total number of knives he has left to throw.  This amount,
where applicable, is displayed in the chat area when a player switches to
his knife.

If a knife hits an enemy, it does a maximum of g_knifeDamage damage.  If
friendly fire is on, it will do the same damage if a knife hits a teammate.
If friendly fire is off, a knife that hits a teammate will instead go towards
that teammate's throwable knives count.

g_maxKnives is the limit to how many knives one can carry at any given time.
Though a player can continue to pick up knives, he will only have up to 
g_maxKnives knives at his disposal.  Setting g_maxKnives to 0 removes this
limit.


UNCAP AMMO - changed: new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_capAmmo		default: 0	enable/disable forced ammo caps

When g_capAmmo is off (0), players' ammo won't reset to the g_maxclips value
when they pick up an ammo pack.  This scenario occurs only for medics and
engineers, who can potentially have more than g_maxclips clips in their reserves
if they pick up dropped weapons.


UNLOCK WEAPONS - new for 3.0
--------------------------------------------------------------------------------
Cvars:
  g_unlockWeapons	default: 2	see documentation

g_unlockWeapons has 3 settings:
  0 - disabled
  1 - medics and engineers can drop their gun and pick up mp40s/thompsons/stens
  2 - any class can drop their gun and pick up any weapon
  3 - venoms/flamethrowers/mausers drop on limbo

Each setting is cumulative.

My hand aches.

================================================================================
7. FEATURES UNCHANGED PRIOR TO 3.0b
================================================================================

ALLOW / DISALLOW SOLDIER GUNS
--------------------------------------------------------------------------------
Cvars:
  g_allowPF		default: 0	allow/disallow panzerfausts
  g_allowFlamer		default: 1	allow/disallow flamethrowers
  g_allowMauser		default: 1	allow/disallow mauser
  g_allowVenom		default: 1	allow/disallow venom gun

When g_allowPF is off (0), players will be unable to select the Panzerfaust as
their weapon.  If they attempt to, they are told that it is not allowed and are
automatically switched to their team's default SMG (Thompson or MP40).

New for 2.03: Flamethrowers, Mausers, Venoms can be selectively disabled using
g_allowFlamer, g_allowMauser, and g_allowVenom, respectively.  By default, those
three variables are on (1).


AMMO GIVING / DONATING - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_giveAmmo		default: 1	enable/disable ammo giving/donating

Commands (client-side):
  giveammo <amount>

When g_giveAmmo is on (1), players can donate spare ammo from their reserves to
other players.  This is accomplished by standing near a player and pointing at
him, followed by issuing the command "/giveammo x", where x is the amount of
ammo he wishes to give.  This is useful in situations where a Lieutenant is not
around, and one player has plenty of ammo to spare.  The giveammo command can be
bound to a key with a pre-determined amount of ammo for added convenience.

Players can only give ammo to players with the same weapon as they one they are
holding when g_giveAmmo is 1.  When g_giveAmmo is 2, players can give ammo
to other types of weapons using exchange rates proportional to the size of
a weapon's clip.  For example, if a player with a Venom wishes to give 500 ammo
to a player with a MP40, the MP40 user will receive 32 bullets.

For both methods, only bullet-based weapons can exchange ammo.


AMMO HINTS - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_ammoHints		default: 1	turn on/off ammo hints
  g_ammoHintDelay	default: 250	time (ms) between hint updates

When g_ammoHints is on (1), Lieutenants will receive a readout of how much ammo
a teammate is carrying when he points at a teammate from a close distance.  This
value will update every g_ammoHintDelay milliseconds.


AMMO PACKS GIVE SYRINGES and PISTOL ROUNDS and HELMETS
--------------------------------------------------------------------------------
Cvars:
  g_ammoGivesSyringe	default: 1	turn on/off syringes in ammo packs
  g_ammoGivesPistol	default: 1	turn on/off pistol rounds in ammo packs
  g_ammoGivesHelmet	default: 1	turn on/off helmets in ammo packs

When g_ammoGivesSyringe is on (1), medics will receive one syringe per ammo
pack that they pick up.  The ten syringe limit still applies.  Similarly, when
g_ammoGivesPistol is on (1), pistol ammo is refilled by ammo packs.

New for 2.05: Ammo packs can return helmets when g_ammoGivesHelmet is on (1).
This can be used in conjunction with helmet protection, detailed elsewhere.


AXIS / ALLIES LEFT
--------------------------------------------------------------------------------
Cvars:
  g_showTeamCount	default: 1	turn team left reporting on/off
  g_teamCountConsole	default: 1	enable/disable team counts in console

When g_teamLeft is on (1), when a player enters limbo mode, the number of axis
and allies left per team is displayed to every player in the center of his/her
screen.  These totals take into account who has respawns left, thus it is
particularly useful for OLTL play.  If g_maxlives is 0, this information is not
displayed, regardless of g_teamLeft.

New for 2.03: When g_teamCountConsole is off (0), team counts are not displayed
in the console.


BINOCULARS FOR NON-LTs - new for 2.05
--------------------------------------------------------------------------------
Cvars:
 g_binocs		default: 0	set which other classes have binocs

g_binocs has 7 modes of binocular distribution:

  1 - all, 2 - soldiers, 3 - medics, 4 - engineers, 5 - medics and soldiers,
  6 - soldiers and engineers, 7 - medics and engineers

A setting of 0 disables this feature.  Only LTs can call for artillery strikes.


CANCEL VOTE
--------------------------------------------------------------------------------
Commands (server-side):
  cancelvote

Cancelvote cancels a vote taking place.


CENSOR CHAT - new for 2.06
--------------------------------------------------------------------------------
Cvars:
 g_censor		default: blank	list of words to censor in chat
 g_censorPenalty	default: 0	turn on/off gibbing for using censored
					words
 g_censorPenaltyMsg	default: *	message to display when censored word is
					used

* g_censorPenaltyMsg defaults to "^1Note: ^7You have been killed for using a
censored word"

When g_censor is set, words in g_censor will be filtered from any chats on the
server.  They will be replaced with asterisks.  For example, if g_censor is set
like so:

  g_censor "swear curse blah"

An attempt to say "I swear I'm going to curse and start swearing again." will
appear as "I **** I'm going to ***** and start ****ing again."  When a filtered
word is encountered, the text is sent with the colors stripped out.  Otherwise,
colors remain intact.

You must issue a map_restart if you make any changes to g_censor in order for
the changes to apply.

When g_censorPenalty is on (1), players explode when they say a censored word.
They are also messaged with g_censorPenaltyMsg, which can be customized.

Note: Both g_censor and g_censorPenaltyMsg must be set in a config file or in
the console.  Attempting to do so through rcon may result in only the first word
you enter being set as that cvar.


CLAN ALIGNMENT - new for 2.06
--------------------------------------------------------------------------------
Commands (server-side):
  clan <team> <clan tag>

Upon issuing the "clan" command with the appropriate parameters, players with
the <clan tag> in their name will be moved to the specified <team>.  Everyone
else will be moved to the opposite team, regardless of team balance.  Spectators
will be ignored.  The team specified can be either axis, red, allies, or blue.
The clan tag must not have any colors and can include spaces.


CLIENT NUMBER FINDER - new for 2.07
--------------------------------------------------------------------------------
Commands (client-side):
  clientnum <match>

This command will return a list of players with <match> in their name, their
score, and, most importantly, their client number.  This is a utility/helper
function to assist in using commands that require client numbers in the event
that you are not running WAB.


CORPSE DRAGGING - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_dragCorpse		default: 1	enable/disable corpse dragging

Commands (client-side):
  +salute

When g_dragCorpse is on (1), players can drag other dead players around by
crouching over the body and holding down the +salute key.  It is recommended
that +salute be bound to a mouse button, which makes dragging much easier.
Corpse dragging is useful in situations where a medic is not nearby and a body
can be stashed in a safe area while a medic arrives.

Alternately, g_dragCorpse can be set to 2 to prevent players from dragging
enemy corpses.


CUSTOMIZE BONUS POINTS VALUES - new for 2.03
-------------------------------------------------------------------------------
Cvars:
  Name		     Default	Function
  g_stealObjBonus	10	bonus for stealing objective
  g_captureObjBonus	15	default bonus for capturing objective
  g_killCarrierBonus	10	bonus for killing objective carrier
  g_secureObjBonus	10	bonus for securing objective from slain carrier
  g_plantBonus		5	bonus for planting dynamite at objective
  g_diffuseBonus	5	bonus for diffusing dynamite
  g_destObjBonus*	-1	bonus for destroying objective with dynamite
  g_capBlueObjBonus*	-1	bonus for capturing allied objective
  g_capRedObjBonus*	-1	bonus for capturing axis objective

  * These three bonuses are special in that the points given are specified in
    the map.  To use the map's default values, set these cVars to -1.  Anything
    above or below -1 will override the map's default values.

These values are fairly self-explanatory, however, note that g_capBlueObjBonus
is added to an allied player who captures an objective for his team.  For
example, an allied player transmits the documents on mp_beach.  Likewise,
g_capRedObjBonus is for axis objectives.

Note that g_captureObjBonus is rarely used.


DISABLE CHECKPOINT FLAG WINS
--------------------------------------------------------------------------------
Cvars:
  g_flagWins		default: 1	enable/disable checkpoint flag wins

When g_flagWins is off (0), checkpoint flags are removed from the map after a
map_restart is called.  This effectively turns any solely checkpoint map into a
team deathmatch, and is not recommended for unlimited life games.


DISABLE CORPSE SINKING - new for 2.02
--------------------------------------------------------------------------------
Cvars:
  g_sinkCorpses		default: 1	turn on/off corpse sinking

When g_sinkCorpses is off (0), corpses will remain on the ground indefinately,
unless gibbed by an explosive or other force.  Note that this setting will only
take affect if g_maxlives is 1.


DISABLE FLAG POINTS
--------------------------------------------------------------------------------
Cvars:
  g_flagPoints		default: 1	allow/disallow flag points

When g_flagPoints is off (0), players do not receive points for retrieving flags.


DISABLE MAP-STARTING VOTES
--------------------------------------------------------------------------------
Cvars:
  g_allowStartVotes	default: 1	allow/disallow map-starting votes
  g_maxStartVotes	default: 3	number of attempts allowed before player
					is kicked

When g_allowStartVotes is off (0), map-starting votes are disallowed.
Specifically, start_match and map_restart are not allowed at any time, and
reset_match is not allowed during warmup (so that reset_match can still be used
during actual round play in the event of a massive, intentional teamkill).  The
number of attempts to call a disallowed vote is tracked, and players are warned
not to call them after attempting to do so.  If the number of attempts meets or
exceeds g_maxStartVotes, the player is kicked.


DISABLE MEDIC SELF-HEALING WHEN CARRYING AN OBJECTIVE - new for 2.04
--------------------------------------------------------------------------------
Cvars:
  g_medObjHeal		default: 1	turn on/off medic self-healing when
					carrying obj

When g_medObjHeal is off (0), medics cannot heal themselves with their health
packs when they are carrying an objective.  They may still pick up enemy health
packs or health packs from other medics.


DISALLOW SPECIFIC VOTES - new for 2.06
--------------------------------------------------------------------------------
Cvars:
  g_disallowedVotes	default: "freezetag headshot normal"

Any vote strings contained in g_disallowedVotes will be denied if a user
attempts to call a vote for them.  For example, g_disallowedVotes can be changed
to:

  g_disallowedVotes "freezetag headshot normal map"

This will deny votes to change the map.  Note that if you wish to use the
"Disable Map-Starting Votes" feature, you must NOT place "map_restart",
"start_match", or "reset_match" in the g_disallowedVotes cVar.


DYNAMITE WARNINGS - new for 2.04
--------------------------------------------------------------------------------
Cvars:
  g_dynaWarn		default: 5	dynamite warning notification time

When g_dynaWarn is on (1), players near a friendly, armed dynamite are warned
that the dynamite will detonate 5 seconds prior to it doing so.

New for 2.05: g_dynaWarn now specifies the number of seconds before a dynamite
blows that teammates are notified.  It defaults to 5 seconds.  When g_dynaWarn
is 0, the feature is disabled.


FORCE RESPAWN - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  respawn <clientnumber>

This command will immediately respawn a player who is either dead or in limbo,
regardless of how many lives the player has left in a g_maxlives game.  It
requires a clientnumber, which can be found with the /clientnum helper function
or used in conjunction with WAB.


FORCE TEAM - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  putteam <team> <clientnumber>

This command will immediately switch someone to the specified team, which can be
either axis/red/r, allies/blue/b, or spectator/spec/s.  It requires a
clientnumber, which can be found with the /clientnum helper function or used in
conjunction with WAB.


FLAMETHROWER SELF-DAMAGE IN FF-OFF MODE - new for 2.06
--------------------------------------------------------------------------------
Cvars:
  g_flameDamage		default: 0	turn on/off flamer self-damage in FF off
					mode

When g_flameDamage is on (1), flamethrowers can damage themselves even if
friendly fire is disabled.  When friendly fire is enabled, flamethrowers can
still damage themselves regardless of g_flameDamage.


FREEZE TAG - new for 2.05
--------------------------------------------------------------------------------
Cvars:
  g_freezeTag		default: 0	turn freeze tag on/off
  g_unFreezeTime	default: 3	time to unfreeze
  g_freezeRespawn	default: 0	unfreeze timeout
  g_unFreezeInvul	default: 1	invulnerability time after unfreeze

When g_freezeTag is on (1), freeze tag mode is enabled upon restarting the map.
In freeze tag play, when a player "dies," he is frozen in place.  A green icon
hovers over the player's head to indicate that he is frozen.  The player will
not be able to move or shoot, but pivot around.

To unfreeze teammates, one must stand next to him for g_unFreezeTime seconds.
A player does not need to be facing the frozen teammate to unfreeze, so he can
simply touch the frozen teammate and face an area of interest to defend himself.
This time must be continuous.  If a player begins to unfreeze someone, then
ventures off and returns later, he will have to stand next to the teammate for
the full g_unFreezetime duration.

Medics can also unfreeze teammates instantly by using a needle, but medics can
still unfreeze "manually" to save needles.

Players receive 1 point for freezing an enemy, -3 points for freezing a team-
mate, 2 points for unfreezing a teammate, and 1 point for unfreezing a teammate
with a needle.

The object of the game is to freeze the entire opposing team.  Objective and
checkpoint wins are still valid.

It is highly recommended that you set g_maxlives to 1.  This will disallow
players from changing classes to become unfrozen.  Note that even though
g_maxlives is 1, players will technically never die.  It is also recommended
that you have enough time during warmup to allow players to switch classes.

If desired, frozen players can be automatically unfrozen after a certain
duration has passed since their initial freezing or a teammates' attempt at
unfreezing them, whichever is greater.  This can be set with g_freezeRespawn.
For example, if g_freezeRespawn is set to 120, players will automatically
unfreeze after two minutes of waiting.  It is not recommended that this
setting be used except in small games.

g_unFreezeInvul controls the number of seconds a player is invulnerable after
being unfrozen.  It defaults to one second.

Logging occurs in the following format:

  Freeze: <frozenNum> <freezerNum> (<method of death num>): <freezerName>
    froze <frozenName>
  Unfreeze: <frozenNum> <freezerNum>: <freezerName> unfroze <frozenName>

End of round stats are logged in the same way as normal play, except gibs
will always be 0.

Frozen players can partially be walked through to prevent problems with
blocking passageways, but this may also make it difficult to unfreeze with
the needle in certain situations.  The needle is also a "proximity unfreeze,"
since normal unfreezing has a proximity effect.  Thus, it is possible to
unfreeze multiple people with one needle.

Note that if you are holding a ticking grenade and are frozen, the grenade will
be frozen along with you.  When you are unfrozen, the grenade will be thrown.

Please report any bugs!

New for 2.06: Implemented fix similar to medic revive unstick fix in 2.05.
Additionally, players cannot drop their weapons while frozen.

Freeze Tag can be voted for if "freezetag" is removed from g_disallowedVotes.
See the documentation for g_disallowedVotes for details.  It is highly
recommended that you also remove "normal" from g_disallowedVotes if you decide
to allow the "freezetag" vote.  Players initiate relevant votes through the
command "/callvote freezetag" or "/callvote normal".

When g_freezeRespawn runs out, players are moved to the respawn queue instead of
unfreezing at their current position.  This works regardless of g_maxlives.


FUN MODE
--------------------------------------------------------------------------------
Cvars:
  g_fun			default: 0	turn fun mode on/off

When g_fun is on (1), the following applies:
  Commands:
    /spawnGuns	drop between 1 to 10 random guns near player
    /spawnNades	drop 10 nades randomly around player in a "nade shower"
    /spawnHats	drop 10 hats randomly around the player

  Additional Features:
    Give Ammo on Demand - players are given one grenade and ammo for their
			  two-handed weapon when calling for ammo
    Give Health on Demand - players are given 20 health when calling for a medic

New for 2.04: Requests for ammo and health are silenced when fun mode is on to
avoid flooding.  Grenades given and PF ammo given has also been increased.

g_allowLTF must also be on (1) for fun mode to be active.  g_allowLTF can only
be disabled in the command line (add "+set g_allowLTF 0" to the command).


GIB REPORTING / GIB POINTS - new for 2.03: now configurable
--------------------------------------------------------------------------------
Cvars:
  g_reportGibs		default: 1 	enable/disable gib reporting
  g_gibPoints		default: 1	enable/disable gib points
  g_showTapOut		default: 0	turn on/off "tapped out into limbo" msgs

When g_reportGibs is on (1), player gibs (when someone transitions from being
revivable to limbo or directly from alive to limbo) are now reported in the
console and server log.  Team gibs are given a red-colored message to
distinguish themselves from enemy gibs.  Server logging of gibs occurs even when
g_reportGibs is off (0).
  FORMAT: Gib: <gibberClientID> <gibbedClientID>: <gibberName> gibbed
	  <gibbedName>

When g_gibPoints is on (1), players receive 1 point for gibbing an enemy and a 3
point deduction for gibbing teammates or gibbing himself.

When g_showTapOut is on (1), "tapped out into limbo" messages are displayed in
the console.


HEADSHOT PRACTICE MODE - new for 2.06
--------------------------------------------------------------------------------
Cvars:
  g_headshot		default: 0	turn headshot mode on/off
  g_headshotDmg		default: 25	in headshot mode, damage per headshot

By popular demand, headshot practice mode has been added with the permission of
the Dead Penguin Clan.  When g_headshot is on (1), only knives or head shots
frombullet weapons do any damage.  Adjusting g_headshotDmg adjusts the amount of
damage each headshot inflicts.  You may want to turn off Helmet Protection.
Setting g_headshotDmg high (e.g. 1000) will gib a player after one head shot.

During head shot mode, hit boxes will be drawn when a player lands a shot on
another player to indicate where one is shooting.  These boxes appear bright
red and fade away after 400 milliseconds, though this can be adjusted on the
client side through cg_railTrailTime.  When a player receives a head shot,
either a smiley face or "LOSER" is printed over his head and the player who
lands the head shot receives an audible and textual confirmation.  The text
can be turned off by setting g_headshot to 2.

Headshot mode is votable if "headshot" is removed from the g_disallowedVotes
string.


HELMET PROTECTS FROM HEAD SHOTS - now customizable for 2.03
--------------------------------------------------------------------------------
Cvars:
  g_helmetProtects	default: 1	turn on/off helmet protection
  g_helmetDamage	default: 5	amount of damage first head shot takes

When g_helmetProtects is on (1), the first head shot a player receives will
do g_helmetDamage damage on the player and knock off his helmet.  The idea here
is that the helmet largely protects a player from his first head shot.

Note that this applies to all shots except those coming from a sniper, where
normal damage still occurs.


IGNORE CLIENTS - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  ignore <clientnumber>
  unignore <clientnumber>

Ignoring a client will silence their chats, voice chats, callvotes, votes, and
private messages.  An ignored client does not count towards the vote totals.
Unignoring a player has the opposite effect.  These commands require a
clientnumber, which can be found using the clientnum helper function or used in
conjunction with WAB.

Players can also call a vote to ignore a client using the vote
"/callvote ignore <client>", where client is either a clientnumber or the name
of the client (partial matches are accepted, colors should not be entered).
Similarly, "/callvote unignore <client>" will allow players to vote on
unignoring a client.  These two votes can be selectively disabled by adding them
to g_disallowedVotes.


IGNORE COMPLAINT FRORMS - new for 2.07
--------------------------------------------------------------------------------
Commands (client-side):
  nocomplaints

A user can type "/nocomplaints 1" in the console to elect not to receive
complaint forms for TKs.  Alternately, complaint forms can be turned back on
with "/nocomplaints 0".  Simply typing "/nocomplaints" can be used to check
whether one is ignoring complaint forms or not.


KILL PLAYER - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  killclient <clientnumber>

This commmand kills a player on command.  It requires a clientnumber.

This command requires g_allowLTF to be on.


LEG SHOT TEMPORARY SPEED REDUCTION - new for 2.04
--------------------------------------------------------------------------------
Cvars:
  g_legDamage		default: 0	turn on/off leg shot speed reduction
  g_legDmgTime		default: 300	duration (in ms) of initial speed
					reduction
  g_legDmgAmount	default: 2	speed reduction constant

When g_legDamage is on (1), a player's speed is temporarily reduced when shot in
the leg or foot.  The reduction in speed is proportional to the ratio of damage
taken to current health and the setting g_legDmgAmount.  The greater the value
of g_legDmgAmount, the greater the speed reduction.  The initial speed reduction
has a duration of g_legDmgTime milliseconds.  After that time has expired, the
speed begins to increase exponentially to the normal value.


LIMIT SOLDIER WEAPONS - new for 2.06
--------------------------------------------------------------------------------
Cvars:
 g_maxFlamer		default: -1	max flamers per team (-1 is unlimited)
 g_maxMauser		default: -1	max mausers per team (-1 is unlimited)
 g_maxPF		default: -1	max panzers per team (-1 is unlimited)
 g_maxVenom		default: -1	max venoms per team (-1 is unlimited)

When g_maxFlamer is 0, flamethrowers are disabled.  If it is greater than 0,
only g_maxFlamer flamethrowers will be allowed.  Players who attempt to select
a flamethrower after g_maxFlamer flamethrowers are already in use, then they
will be notified of such and given an SMG instead.

The functionality of the other three cVars is similar.  Note that this is on a
per-team basis.


ORIENT / DISORIENT / SHAKE PLAYER - new for 2.07s
--------------------------------------------------------------------------------
Commands (server-side):
  orient <clientnumber>
  disorient <clientnumber>
  shake <clientnumber>

Disorienting a client has a random outcome, but it will either mix their
directional controls around, flip their view upside-down, or a combination of
both.  Doing it multiple times is encouraged.  Orienting a player reverts
his controls and view to its normal state.  Shaking a player wobbles his display
for a short time.  These functions require a clientnumber, which can be found
with the /clientnum helper function or used in conjunction with WAB.

These commands require g_allowLTF to be on.


PLAY DEAD - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_playDead		default: 0	enable/disable playing dead
  g_playDeadLook	default: 1	enable/disable looking around while PD

Commands (client-side):
  playdead

When g_playDead is on (1), players who issue the command "playdead" will drop to
the ground and remain motionless, essentially feigning death.  If g_playDeadLook
is on (1), players can adjust their view angles to survey their surroundings,
otherwise it is locked at the same angle they entered the playing dead state
from.  Players must remain playing dead for at least 2.5 seconds before they
can get back up.  Players get back up using the "playdead" command again.  When
they stand up, they are locked for 3 seconds.

You cannot play dead while priming a nade.  When you are playing dead, your
"corpse" cannot be walked through, thus assisting others in identifying who is
playing dead, but your corpse can be jumped over.  This feature is particularly
fun in One Life to Live mode with g_sinkCorpses off.


PRINT COMMANDS - new for 2.07
--------------------------------------------------------------------------------
Commands (server-side):
  chat <message>
  cp <message>
  print <message>

Chat, cp, and print write text to the chat area, center of the screen, and
console, respectively.  Text from these commands is not preceeded by "Console:",
and you may use \n to add a line break for chat and print.


PROPERTY DAMAGE - new for 2.05
--------------------------------------------------------------------------------
Cvars:
  g_propDamage		default: 0	turn on/off property damage

When g_propDamage is on (1), the amount of property damage each player does is
tracked.  E.g., if someone breaks a window or vase, that counts as property
damage.  These values are pulled from the item's density, and randomly generated
for exploding barrels, statues, and chairs in a pre-determined range.  At the
end of the round, the player with the most property damage is recognized in the
stats printout.


REVIVE UNDERWATER - new for 2.06
--------------------------------------------------------------------------------
Cvars:
 g_waterRevive		default: 0	turn on/off underwater reviving

When g_waterRevive is on (1), medics can revive players underwater.


ROTATING SERVER MESSAGES / MOTDs - new for 2.05
--------------------------------------------------------------------------------
  g_showMsgs		default: 0	turn off msgs/seconds between each msg
  g_msgPos		default: 0	position of message
  g_msg1...g_msg10	default: blank	messages

When g_showMsgs is 0, this feature is off.  Any integer above 0 is considered to
be the number of seconds between each message.  A value of 300 is recommended,
corresponding to 5 minutes per message.  When a delay is set with g_showMsgs,
the text contained in g_msg1 through g_msg10 is cycled on users' screens.  Not
every g_msg<n> slot needs to be filled, but they must be contiguous.  If you
only wish to have two messages, you must use g_msg1 and g_msg2 and leave the
rest blank.  It is highly recommended that you set these values in your server
configuration file instead of in the console.  Note that you can user colors and
spaces if you surround the actual message in quotes, e.g.

  set g_msg1		"This server is running ^3shrub mod^7!"
  set g_msg2		"^4www.planetwolfenstein.com/shrub"
  set g_showMsgs	300

By default, the messages are displayed in the lower-left corner of the screen,
where chat and voice commands are normally displayed.  This can be changed with
g_msgPos.  Three options are available:

  0 - bottom left, 1 - center, 2 - top left/in console

The rotation is persistant across map rotations, so it will not reset to the
first message when you start a new map.


SHOW REVIVES - new for 2.03
--------------------------------------------------------------------------------
Cvars:
  g_showRevives		default: 0	turn on/off printing revives in console

When g_showRevives is on (1), revives are printed in the console in the format:
<revivedName> was revived by <reviverName>


SHUFFLE TEAMS & AUTO TEAM SHUFFLING - new for 2.06
--------------------------------------------------------------------------------
Cvars:
  g_shuffle		default: 0

Commands (server-side):
  shuffle

When g_shuffle is greater than 0, the teams will automatically be shuffled
whenever one team has g_shuffle more wins than the other team.  Swap_teams are
accounted for.  When g_shuffle is 0, no automatic shuffling takes place.  The
shuffles are completely random and not based on prior performance.

Admins can call and immediate shuffle through the command "shuffle".

Players can call a vote to shuffle teams using /callvote shuffle if "shuffle"
is removed from g_disallowedVotes.


SPECTATORS CAN TALK TO NON-SPECS - new for 2.02
--------------------------------------------------------------------------------
Cvars:
  g_allowSpecChat	default: 1	turn on/off spec chat to non-specs

When g_allowSpecChat is on (1), spectator chat using the "say" command will also
be relayed to non-spectator players, i.e. axis and allies.


START OF ROUND AMMO REQUEST PENALTY - new for 2.04
--------------------------------------------------------------------------------
Cvars:
  g_startAmmoPenalty	default: 5	start ammo penalty time

When g_startAmmoPenalty is on (1), anyone who calls for ammo within the first 5
seconds of a round start receives an ammo reduction (20 for flamer, 50 for venom,
3 for others).  Their request for ammo is also not broadcast.

New for 2.05: g_startAmmoPenalty now specifies the number of seconds after the
start of the round in which the penalty applies.  When g_startAmmoPenalty is 0,
the feature is disabled.


STATISTICS
--------------------------------------------------------------------------------
Cvars:
  g_showStats		default: 1	turn limbo stats on/off
  g_logStats		default: 1	log round stats enable/disable
  g_lifeStats		default: 1	use life stats (1) or round stats (0)

Commands (client-side):
  stats

When g_showStats is on (1), upon entering limbo mode, players will be shown the
number of kills and revives (if applicable) they made during that life.
Additionally, the accuracy percentage (shots landed / shots fired) of any bullet
weapons used is displayed.  Note that accuracy is still displayed for flamers
since they have a pistol and the ability to switch to a venom, mauser, or SMG.

New for 2.02: Stats for the entire round (including team kills, gibs, team gibs,
shots fired, shots hit, head shots, ammo given, and health given, in addition to
the above stats) are now printed in the console at the end of the round for each
player.  This feature cannot be turned on/off.  Current stats for the round may
be viewed at any time using the client command "stats".

New for 2.03: There seems to be some confusion over the life stats that are
displayed during limbo.  These particular stats are reset whenever a player dies.
This, if a player kills someone, dies, is revived, dies again, and goes into
limbo, his stats will display zero kills since he died and did not kill anyone
after starting a new life by being respawned.  However, that kill still remains
counted in the round stats, which are displayed at the end of the round and
accessable any time with /stats.  With this in mind, if players do not prefer
life stats, admins now have the option of setting g_lifeStats to zero (0), which
will display the round stats during limbo.

When g_logStats is on (1), stats are sent to the server log at the end of the
round for all non-late joiners in the following format:

ENDROUND: <clientID>: <name>: <lastClass> <numKilled> <numTKed> <numGibbed>
          <numTGibbed> <numRevived> <healthGiven> <ammoGiven>
          <shotsFired> <shotsLanded> <headShotsLanded>

Note that lastClass can be either: 0 - Sold, 1 - Med, 2 - Engr, 3 - LT

New for 2.04: The number of times one has been killed and the kill ratio for
that round is now tracked and reported via /stats or the end-of-round stats.

New for 2.05: The player with the most kills and the player with the most
revives are recognized at the end of the round in the stats printout.


TOUCH GIB
--------------------------------------------------------------------------------
Cvars:
  g_touchgib		default: 0	turn touch gibbing on/off

When g_touchgib is on (1), players can gib others instantly by merely touching.
Weapons are disabled, so a map_restart is recommended after setting g_touchgib
to 1 in order to clear weapons from the game.  After map_restart, touch gibbing
is enabled 10 seconds after the map is restarted to allow time for players to
disperse.  If players are facing each other when they touch, there is a high
probability that they will gib each other.

Note: This is much more fun when respawn times are removed.  To do this, use:
  g_useralliedrespawntime 1
  g_useraxisrespawntime 1
To reset respawn times, set them both to 0.

New for 2.05: When g_touchgib is set to 2, you cannot touch gib a teammate.


WARMUP FREEZING - new for 2.07
--------------------------------------------------------------------------------
Cvars:
  g_warmupFreeze	default: 0	enable/disable warmup freezing

When g_warmupFreeze is on (1), players are frozen during warmup.  This
encourages planning strategies during warmup, particularly for one-life servers
with long warmup times.


NON-MODIFIABLE/INTERACTIVE FEATURES
--------------------------------------------------------------------------------
If you are an admin who would like to turn off one of these features, contact me
and I'll make it customizable in the next build.
--------------------------------------------------------------------------------

LT Ammo Distribution Prioritizer - new for 2.03
  When an LT says "I'm a Lieutenant" to his team, he is told who the best person
  to give ammo to based on need and distance from the LT.

Medic Reminders - new for 2.02
  Moments after a player dies, the nearest, living medic is told that the player
  is down and needs a revive.  This process is repeated every 10 seconds.

  New for 2.03: The location of the downed player is now sent.

No Medic Reminders - new for 2.02
  If a player dies but is revivable, and there is no medic left on his team in
  OLTL mode, he will be reminded of such.

No LT Reminders - new for 2.03
  If a player requests ammo and no LT is alive or has respawns left, the player
  is told that no LTs are left.

Don't Drop Client if Down
  If a player is down and waiting for a revive, the server will not drop him/her
  for inactivity.  Thus, g_inactivity can still be used without worrying about
  its effect on limited-life games.

Revive Logging
  Revives are now logged in the server log.
  FORMAT: Revive: <reviverClientID> <revivedClientID>: <reviverName> revived
          <revivedName>

30 second team-change wait is disabled
  Players no longer need to wait 30 seconds before joining or switching teams.

  New for 2.03: Delay is back, with a 5 second requirement between team switches
  to avoid floods.

/kill logging
  /kills are not logged as actual kills, but are logged in the following format:
  Suicide: <clientNum>: <clientName>

Black text can be used at beginning of name - new for 2.04
  Black text can now be used at the beginning of a name.  Preceeding black text
  with a karat is no longer necessary.

Players can switch teams during warmup - new for 2.04
  Regardless of the g_maxlives setting, players can always switch teams during
  the warmup period.

Fixed "getting stuck in something after being revived" bug - new for 2.05
  If you've ever been stuck under the trains in depot or in the boxes on base
  (and who hasn't?), you'll be pleased to know that it will no longer occur.

Engineer Logging - new for 2.05
  When an engineer repairs an MG42 that he has not destroyed, it is logged:
  EVENTREPAIR: <clientNum>: <clientName> repaired an MG42
  Similarly, when an engineer blows an objective, it is logged:
  EVENTTNT: <clientNum>: <clientName> destroyed an objective

Vote Logging - new for 2.07
  Vote strings are now displayed in the "called a vote" line.

================================================================================
8. KNOWN BUGS
================================================================================

 - detailed stats table is misaligned when two-digit kill ratios are displayed
 - certain heads might not appear when g_disguise is enabled

================================================================================
9. LICENSE
================================================================================
You may use the included .DLL or .SO file with your RTCW server.  You may
distribute the .DLL or .SO file if and only if this text file is included.  You
may not make modifications to the .DLL or .SO file.  You may not hold "shrub"
responsible for any damage of any kind resulting from the use of the included
.DLL or .SO file.


END OF FILE