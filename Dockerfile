FROM msh100/rtcw

ADD shrubmod.tar.gz /home/game/

EXPOSE 27960/udp

ENTRYPOINT ["/home/game/start"]
